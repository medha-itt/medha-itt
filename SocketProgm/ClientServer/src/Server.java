import java.net.*;
import java.io.*;

public class Server {
	private Socket clientSocket = null; 
    private ServerSocket server = null; 
    private DataInputStream inputStream = null; 
    private DataOutputStream outputStream = null;
  
    public Server(int port) throws IOException 
    {
    	server = new ServerSocket(port); 
        System.out.println("This is the server.");
        System.out.println("Waiting for a client ..."); 
        
        clientSocket = server.accept(); 
        System.out.println("Client accepted!");
        
        inputStream = new DataInputStream( 
        new BufferedInputStream(clientSocket.getInputStream())); 
        
        String clientMsg = inputStream.readUTF();
        System.out.println(clientMsg); 
        
        outputStream = new DataOutputStream(clientSocket.getOutputStream()); 
	    String serverMsg = "Hello, client. This is server!"; 	    
	    outputStream.writeUTF(serverMsg); 
	    outputStream.flush(); 
        
     	System.out.println("Closing connection");  
     	inputStream.close();
     	outputStream.close();
     	server.close();
    }
    
    public static void main(String args[]) throws IOException
    { 
        Server server = new Server(4675); 
    } 
}

