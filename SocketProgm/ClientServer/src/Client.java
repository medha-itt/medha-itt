import java.net.*;
import java.io.*;

public class Client {
	private Socket clientSocket = null; 
    private DataInputStream  inputStream = null; 
    private DataOutputStream outputStream = null;
    private DataInputStream serverSideMsg = null;
    
    public Client(String address, int port) throws IOException, UnknownHostException
    {  
    	clientSocket = new Socket(address, port); 
	    System.out.println("Client is connected"); 
	
	    inputStream = new DataInputStream(System.in);
	    outputStream = new DataOutputStream(clientSocket.getOutputStream()); 
    
	    String inputMsg = "Hello, server. Client here!"; 	    
	    outputStream.writeUTF(inputMsg); 
	    outputStream.flush(); 
		
		
		serverSideMsg = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream())); 
		String serverMsg = serverSideMsg.readUTF();
		System.out.println(serverMsg);
		outputStream.close();
		inputStream.close();
		clientSocket.close();
 
    }
    
    public static void main(String args[]) throws IOException
    { 
        Client client = new Client("localhost", 4675); 
    } 
}
