import static org.junit.Assert.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import ITTDb.ITTDatabase;
import ITTDb.ITTDbExceptions;
import ITTDb.Customer;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class RetrieveTest {
	ITTDatabase ittCRUD = new ITTDatabase();
    @Before
    public void initialize() throws IOException {
        File file = new File("Customer.json");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[{\"id\":1,\"name\":\"Neha\",\"city\":\"Lucknow\"}]");
        fileWriter.close();
    }

    @Test
    public <T> void retrieveTest() throws IOException, ITTDbExceptions {
        Customer testCustomer = new Customer(1,"Neha","Lucknow");
        List<T> retrievedObjects = (List<T>) ittCRUD.retrieve(testCustomer, "name", "Neha");
        String actual = "[{id:" + 1 + ", name:" + "Neha" + ", city:" + "Lucknow}]";
        assertEquals(actual, new String(Files.readAllBytes(Paths.get("Customer.json"))));
    }

    @After
    public void cleanup() throws IOException {
        PrintWriter writer = new PrintWriter("Customer.json");
        writer.print("");
        writer.close();
    }
}