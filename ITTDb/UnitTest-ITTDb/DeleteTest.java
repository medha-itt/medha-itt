import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import ITTDb.ITTDatabase;
import ITTDb.ITTDbExceptions;
import ITTDb.Customer;

public class DeleteTest {
    @Before
    public void initialize() throws IOException {
        File file = new File("Customer.json");
		FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[{\"id\":1,\"name\":\"Neha\",\"city\":\"Lucknow\"}]");
        fileWriter.close();
    }

    @Test
    public void retrieveTest() throws IOException, ITTDbExceptions {
        Customer testCustomer = new Customer(1, "Medha", "Lucknow");
        ITTDatabase.delete(testCustomer, "id", "1");
        assertEquals("", new String(Files.readAllBytes(Paths.get("Customer.json"))));
    }
}