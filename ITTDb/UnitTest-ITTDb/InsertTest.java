import ITTDb.ITTDatabase;
import ITTDb.ITTDbExceptions;
import ITTDb.Customer;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.*;

public class InsertTest {
	ITTDatabase ittCRUD = new ITTDatabase();

        @Test
        public void insertTest() throws IOException, ITTDbExceptions {
                // Arrange
                Customer testCustomer = new Customer(1, "Neha", "Lucknow");
                String actual = "[{id:" + testCustomer.id + ", name:" + testCustomer.name + ", city:" + testCustomer.city + "}]";
                // Act
                ittCRUD.insert(testCustomer);
                // Assert
                assertEquals(actual, new String(Files.readAllBytes(Paths.get("Customer.json"))));
        }

        @After
        public void cleanup() throws IOException {
                PrintWriter writer = new PrintWriter("Customer.json");
                writer.print("");
                writer.close();
        }

}