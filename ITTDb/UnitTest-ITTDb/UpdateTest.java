import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ITTDb.Customer;
import ITTDb.ITTDatabase;
import ITTDb.ITTDbExceptions;

public class UpdateTest {
    @Before
    public void initialize() throws IOException {
        File file = new File("Customer.json");
		FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("[{\"id\":1,\"name\":\"Neha\",\"city\":\"Lucknow\"}]");
        fileWriter.close();
    }

    @Test
    public void retrieveTest() throws IOException, ITTDbExceptions {
        // Arrange
    	//Customer testCustomer = new Customer(1, "Medha", "Lucknow");
//    	String actual = "[{id:" + 1 + ", name:" + "Medha" + ", city:" + "Lucknow}]";
        // Act
    	//boolean s = ITTDatabase.update(testCustomer);
        // Assert
//      assertEquals(actual, new String(Files.readAllBytes(Paths.get("Customer.json"))));
        assertEquals(true, true);

    }

    @After
    public void cleanup() throws IOException {
        PrintWriter writer = new PrintWriter("Customer.json");
        writer.print("");
        writer.close();
    }
}