package com.medha.ITTDB;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FilesHandler {
	private static JsonParser jsonParser;
	private static JsonGenerator jsonGenerator;
	private static ObjectMapper objectMapper = new ObjectMapper();

	public static <T> String createFileName(T object) throws IOException {
		String fileName = object.getClass().getSimpleName() + ".json";
		return fileName;
	}

	public static boolean doesFileExist(File file) throws IOException{
		if (file.createNewFile()) {
			return false;
		}
		else{
			return true;
		}
	}

	public static <T> List<T> read(String fileName, Class<T> className) throws IOException {
		List<T> fileObjects = new ArrayList();
		if (!checkIfFileEmpty(fileName)) {
			try (FileInputStream inputStream = new FileInputStream(fileName)) {
				jsonParser = objectMapper.getFactory().createParser(inputStream);
				fileObjects = objectMapper.readValue(jsonParser, objectMapper.getTypeFactory().constructCollectionType(List.class, className));
				return fileObjects;
			} catch (JsonMappingException ex) {
				System.out.print("Error occurred:" + ex.getMessage());
			} catch (IOException ex) {
				System.out.print("Error occurred:" + ex.getMessage());
			}
		} else {
			return fileObjects;
		}
		return fileObjects;
	}

	public static <T> void write(String fileName, Object object) throws IOException {
		try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
			jsonGenerator = objectMapper.getFactory().createGenerator(outputStream);
			objectMapper.writerWithDefaultPrettyPrinter().writeValue(jsonGenerator, object);
		} catch (JsonGenerationException ex) {
			System.out.print("Error occurred:" + ex.getMessage());
		} catch (IOException ex) {
			System.out.print("Error occurred:" + ex.getMessage());
		}
	}

	public static boolean checkIfFileEmpty(String fileName) throws IOException {
		File file = new File(fileName);
		if (file.length() == 0) {
			return true;
		} else {
			return false;
		}
	}
}