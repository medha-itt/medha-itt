package com.medha.ITTDB;
import java.io.IOException;
import java.util.List;

public class ObjectDb {
	public <T> void save(T object) throws IOException, ITTDbExceptions {
		ITTDatabase.insert(object);
	}

	public <T> List<T> find(T object, String key, String value) throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		return ITTDatabase.retrieve(object, key, value);
	}

	public <T> boolean update(T object) throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		boolean isUpdated= ITTDatabase.update(object);
		return isUpdated;
	}

	public <T> void delete(T object, String key, String value) throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		ITTDatabase.delete(object, key, value);
	}
}