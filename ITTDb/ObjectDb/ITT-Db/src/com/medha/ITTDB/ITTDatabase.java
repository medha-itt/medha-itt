package com.medha.ITTDB;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ITTDatabase {
	static String fileName;

	@SuppressWarnings("unchecked")
	public static <T> void insert(T object) throws IOException, ITTDbExceptions {
		ArrayList<T> fileObjects = new ArrayList<T>();
		String fileName = FilesHandler.createFileName(object);
		File file = new File(fileName);
		if (FilesHandler.doesFileExist(file)) {
			fileObjects = (ArrayList<T>) FilesHandler.read(fileName, object.getClass());
		}
		fileObjects.add(object);
		FilesHandler.write(fileName, fileObjects);
	}

	public static <T> List<T> retrieve(T object, String key, String value) throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
		String fileName = FilesHandler.createFileName(object);
		@SuppressWarnings("unchecked")
		List<T> fileObjects = (List<T>) FilesHandler.read(fileName, object.getClass());
		List<T> retrievedObjects = (List<T>) ListHandler.filterObjectList(key, value, fileObjects);
		return retrievedObjects;
	}

	public static <T> boolean update(T object) throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		boolean isUpdated = false;
		String fileName = FilesHandler.createFileName(object);
		List<T> objectsList = (List<T>) FilesHandler.read(fileName, object.getClass());
		List<T> objectFound = ListHandler.searchObject(objectsList, object);
		for (int i=0; i < objectsList.size(); i++) {
			T TempObject = objectsList.get(i);
			if(objectFound.get(0).getClass().getDeclaredField("id").get(objectFound.get(0)).equals(TempObject.getClass().getDeclaredField("id").get(TempObject))){
				if(!objectFound.isEmpty()) {
					objectsList.set(i, object);	
				}		
			}
		}
		if(!objectFound.isEmpty()) {
			FilesHandler.write(fileName, objectsList);
			isUpdated = true;
		}
		return isUpdated;
	}

	public static <T> void delete(T object, String key, String value) throws IOException, ITTDbExceptions, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		T deletedObject = null;
		String fileName = FilesHandler.createFileName(object);
		List<T> objectsList = (List<T>) FilesHandler.read(fileName, object.getClass());		
			deletedObject = ListHandler.removeObject(fileName, objectsList, object);
		if (!deletedObject.equals(null)) {
			System.out.println("Record deleted successfully");
		} else {
			System.out.println("No object found to delete");
		}
	}
}