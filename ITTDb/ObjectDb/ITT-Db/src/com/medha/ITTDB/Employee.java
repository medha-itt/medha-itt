package com.medha.ITTDB;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Employee {
	public int id;
	public String name;
	public String designation;

	@JsonCreator
	public Employee(@JsonProperty("id")int id, @JsonProperty("name")String name, @JsonProperty("designation")String designation) {
		this.id = id;
		this.name = name;
		this.designation = designation;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDesignation() {
		return designation;
	}

}