package com.medha.ITTDB;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer {
	public int id;
	public String name;
	public String city;
	@JsonCreator
	public Customer(@JsonProperty("id")int id, @JsonProperty("name")String name,@JsonProperty("city") String city) {
		this.id = id;
		this.name = name;
		this.city = city;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

}