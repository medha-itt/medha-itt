package com.medha.ITTDB;

import java.util.List;

public class Main {
	public static void main(String args[]) throws Exception {

		Customer customer = new Customer(1,"Vidya","New York");

		Employee employee = new Employee(1,"Medha","Software Engineer");

		ObjectDb objectDb = new ObjectDb();
//		objectDb.save(customer);
    	objectDb.save(employee);
         
//		List<Customer> customersFound = objectDb.find(customer, "name", "Vidya");
//		if (customersFound.size()>0)
//		{
//			System.out.println("Successfully found "+ customersFound.size() + " records");
//		}
//		else
//		{
//			System.out.println("Record not found.");
//		}

//		employee.setName("Neha");
//		employee.setDesignation("HR");
//
//		boolean isUpdated = objectDb.update(employee);
//		if (isUpdated)
//		{
//			System.out.println("Record updated successfully.");
//		}
//		else
//		{
//			System.out.println("Record to be updated was not found.");
//		}

		objectDb.delete(customer,"id","1");	
	}
}
