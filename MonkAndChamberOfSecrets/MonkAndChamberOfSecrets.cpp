#include <bits/stdc++.h>
//#incluse <queue>
using namespace std;

int main()
{
    int numOfSpiders , selectedSpiders, maxPower = 0 ;
    int originalSpiderIndex, spider, iterations;
    queue<int> spiderQ ;
    
    //Enter no. of spiders(N) and spiders to be selected(X)
    cin>> numOfSpiders >> selectedSpiders;
    //Enter array of spiders power

    int powerArr[numOfSpiders+1];
    //Input power of spiders
    for (int i=1; i<=numOfSpiders; i++)
    {
     cin >> powerArr[i];
    }
	
    for(int j=1; j<=numOfSpiders; j++)
	{
	    spiderQ.push(j);
	}
    
    //boolean array for evaluated spiders
    bool evaluatedSpidersArray[numOfSpiders+1];
    //initializing the array with false
    memset(evaluatedSpidersArray,false,sizeof(evaluatedSpidersArray));
    
    int count = 0;
    int spidersLeft = numOfSpiders;
    
	for (int i=0;i<selectedSpiders;i++)
    {
     // initial count of evaluated spiders=0  
     count = 0;
     iterations = min(selectedSpiders,spidersLeft);
     maxPower = -1;
        //if number of iterations are left, perform evaluation   
        while (count < iterations)
            {
                //first spider in queue    
                spider = spiderQ.front();
                //pop if spider is already evaluated
                if (evaluatedSpidersArray[spider]) 
                    {
                    spiderQ.pop();
                    continue;
                    }
                //finding maximum power spider from the queue
                if (powerArr[spider] > maxPower)
                    {
                    maxPower = powerArr[spider];
                    originalSpiderIndex = spider;
                    }
            //dequeue first spider in queue
            spiderQ.pop();
            //enqueuing the spider back
            spiderQ.push(spider);
            //increment evaluated spider count
            count++;
            //decreasing power of each selected spider by 1
                if (powerArr[spider])
                powerArr[spider]-=1;
            }
			         
    //making spider evaluated in array
     evaluatedSpidersArray[originalSpiderIndex] = true;
    //output original index of spider with maximum power in the selected queue
     cout<<originalSpiderIndex<<" ";
     //decrement count of spiders left in queue
     spidersLeft--;                                                          
    }
    cout<<endl ;
    return 0;
}

